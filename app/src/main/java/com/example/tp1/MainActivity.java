package com.example.tp1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_recycler_view);

        final RecyclerView rv = (RecyclerView) findViewById(R.id.recyclerView);

        rv.setLayoutManager(new LinearLayoutManager(this));
        rv.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        rv.setAdapter(new IconicAdapter());
    }

    class IconicAdapter extends RecyclerView.Adapter<RowHolder> {
        @Override
        public RowHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return (new RowHolder(getLayoutInflater().inflate(R.layout.row, parent, false)));
        }

        @Override
        public void onBindViewHolder(RowHolder holder, int position) {
            holder.bindModel(AnimalList.getNameArray()[position]);
        }

        @Override
        public int getItemCount() {
            return (AnimalList.getNameArray().length);
        }
    }


    class RowHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        TextView label;
        ImageView icon;

        RowHolder(View row) {
            super(row);
            label = row.findViewById(R.id.label);
            icon = row.findViewById(R.id.icon);
            label.setOnClickListener(this);
            icon.setOnClickListener(this);
        }



    @Override
    public void onClick(View v) {
        // Do something in response to the click
        final String item = label.getText().toString();

        Intent intent = new Intent(MainActivity.this, AnimalActivity.class);

        Animal animal =  AnimalList.getAnimal(item);
        intent.putExtra("animalClass", animal);
        Toast.makeText(MainActivity.this, "Vous avez sélectionné: " + item, Toast.LENGTH_LONG).show();
        startActivity(intent);
    }

    void bindModel(String item) {
        label.setText(item);
        Animal animal = AnimalList.getAnimal(label.getText().toString());
        String animalImg = animal.getImgFile();
        int id = getResources().getIdentifier("com.example.tp1:drawable/" + animalImg, null, null);
        icon.setImageResource(id);
        }
    }
}



        //Partie ListView


        /*

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final ListView listview = (ListView) findViewById(R.id.lstView);
        String[] values = AnimalList.getNameArray();

        // Create an ArrayAdapter, that will actually make the Strings above appear in the ListView
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, values);
        listview.setAdapter(adapter);

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView parent, View v, int position, long id) {
                Intent intent = new Intent(parent.getContext(), AnimalActivity.class);
                final String item = (String) parent.getItemAtPosition(position);
                Animal animal =  AnimalList.getAnimal(item);
                intent.putExtra("animalClass", animal);

                Toast.makeText(MainActivity.this, "Vous avez sélectionné: " + item, Toast.LENGTH_LONG).show();
                startActivity(intent);
            }
        });

        */

