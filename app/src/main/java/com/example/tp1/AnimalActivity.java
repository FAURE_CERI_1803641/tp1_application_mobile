package com.example.tp1;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

public class AnimalActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_animal2);

        Intent i = getIntent();

        final Animal animal = (Animal)i.getSerializableExtra("animalClass");

        String animalImg = animal.getImgFile();
        int id = getResources().getIdentifier("com.example.tp1:drawable/" + animalImg, null, null);

        TextView nomAnimal = findViewById(R.id.nomAnimal);

        ImageView avatar = findViewById(R.id.avatarAnimal);

        TextView valeurEsperanceDeVieMax = findViewById(R.id.valeurEsperanceDeVieMax);

        TextView valeurPeriodeDeGestation = findViewById(R.id.valeurPeriodeDeGestation);

        TextView valeurPoidsALaNaissance = findViewById(R.id.valeurPoidsALaNaissance);

        TextView valeurPoidsALAgeAdulte = findViewById(R.id.valeurPoidsALAgeAdulte);

        final EditText statut = findViewById(R.id.statut);

        valeurEsperanceDeVieMax.setText(animal.getStrHightestLifespan());

        valeurPeriodeDeGestation.setText(animal.getStrGestationPeriod());

        valeurPoidsALaNaissance.setText(animal.getStrBirthWeight());

        valeurPoidsALAgeAdulte.setText(animal.getStrAdultWeight());

        statut.setText(animal.getConservationStatus());

        Button sauvegarde = findViewById(R.id.sauvegarde);


        sauvegarde.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                animal.setConservationStatus(statut.getText().toString());
                finish();

            }
        });

        avatar.setImageResource(id);


    }
}
